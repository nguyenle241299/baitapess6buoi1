let DTB = (...diem) => {
    // diem.forEach((item) => {
    //     sum += item;
    // })
    let sum = 0;
    for(var i = 0; i < diem.length; i++){
        sum += diem[i];
    }
    return sum / diem.length;
}

let tinhDTBKhoiLop1 = () => {
    let diemToan = document.getElementById("inpToan").value * 1;
    let diemLy = document.getElementById("inpLy").value * 1;
    let diemHoa = document.getElementById("inpHoa").value * 1;

    let result = DTB(diemToan, diemLy, diemHoa);

    document.getElementById("tbKhoi1").innerHTML = result;
}

let tinhDTBKhoiLop2 = () => {
    let diemVan = document.getElementById("inpVan").value * 1;
    let diemSu = document.getElementById("inpSu").value * 1;
    let diemDia = document.getElementById("inpDia").value * 1;
    let diemTiengAnh = document.getElementById("inpTiengAnh").value * 1;

    let result = DTB(diemVan, diemSu, diemDia, diemTiengAnh);

    document.getElementById("tbKhoi2").innerHTML = result;
}
