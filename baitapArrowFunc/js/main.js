const colorList = ["pallet", "viridian", "pewter", "cerulean", "vermillion", "lavender", "celadon", "saffron", "fuschia", "cinnabar"];
let colorCurrent= ""
let showColorLists = () => {    
    let contentHTML = "";

    for(let i = 0; i < colorList.length; i++) {
        let currentColor = colorList[i];

        let div = `
            <div id="colorContainer" class="color-button-container">   
                <button onclick = "changeColor('pallet')" class = "color-button pallet"></button>          
                <button onclick = "changeColor('viridian')" class = "color-button viridian"></button>          
                <button onclick = "changeColor('pewter')" class = "color-button pewter"></button>          
                <button onclick = "changeColor('cerulean')" class = "color-button cerulean"></button>          
                <button onclick = "changeColor('vermillion')" class = "color-button vermillion"></button>          
                <button onclick = "changeColor('lavender')" class = "color-button lavender"></button>          
                <button onclick = "changeColor('celadon')" class = "color-button celadon"></button>          
                <button onclick = "changeColor('saffron')" class = "color-button saffron"></button>          
                <button onclick = "changeColor('fuschia')" class = "color-button fuschia"></button>          
                <button onclick = "changeColor('cinnabar')" class = "color-button cinnabar"></button>          
            </div>
        `

        contentHTML += div;
        break;
    }

    document.getElementById("colorContainer").innerHTML = contentHTML;
}

window.onload = showColorLists;

let changeColor = (color) => {
    if(colorCurrent != "")
    {
        document.getElementById("house").classList.remove(colorCurrent);
    }
    colorCurrent=color;
    document.getElementById("house").classList.add(color);
}
